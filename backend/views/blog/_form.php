<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use yii\helpers\Url;

/* @var $this yii\web\View */
/* @var $model common\models\Blog */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="blog-form">

    <?php $form = ActiveForm::begin(); ?>


    <div class="col-xs-12">
        <div class="col-xs-9">
            <?= $form->field($model, 'is_status')
                ->widget(
                    \kartik\switchinput\SwitchInput::classname(),
                    [
                        'value' => true,
                        'pluginOptions' =>
                            [
                                'size' => 'large',
                                'onColor' => 'success',
                                'offColor' => 'danger',
                                'onText' => Yii::t('app', 'Active'),
                                'offText' => Yii::t('app', 'Inactive'),
                            ],
                    ]
                )
            ;
            ?>
        </div>
        <div class="col-xs-3 ">
            <?= $form->field($model, 'position')->textInput(['maxlength' => true]) ?>
        </div>
    </div>


    <div class="col-xs-12">
        <h3> SEO</h3>
    </div>

    <div class="col-xs-12" style="border: 3px groove; margin-bottom: 50px;">
        <div class="row">
            <div class="col-xs-6">
                <?= $form->field($model, 'seo_title')->textarea(['row' => 3]) ?>
            </div>
            <div class="col-xs-6">
                <?= $form->field($model, 'seo_keywords')->textarea(['row' => 3]) ?>
            </div>
            <div class="col-xs-12">
                <?= $form->field($model, 'seo_description')->textarea(['row' => 3]) ?>
            </div>
        </div>
    </div>


    <div class="col-xs-12">
        <div class="row">
            <div class="col-xs-6">
                <?= $form->field($model, 'name')->textInput(['maxlength' => true]) ?>
            </div>
            <div class="col-xs-6">
                <?= $form->field($model, 'short_description')->textarea(['row' => 3]) ?>

            </div>

        </div>
    </div>


    <div class="col-xs-12">
        <?= $form->field($model, 'description')
            ->widget(
                yii\redactor\widgets\Redactor::className(),
                [
                    'clientOptions' =>
                        [
                            'imageUpload' => \yii\helpers\Url::to(['/redactor/upload/image']),
                            'fileUpload' => false,
                            'plugins' => ['fontcolor', 'imagemanager', 'table', 'undoredo', 'clips', 'fullscreen'],
                        ],
                ]
            )
        ; ?>
    </div>

    <div class="col-xs-12">
        <?php

        $modelId = $model->id;
        $uploadImagesUrl = Url::to(['/blog/upload-images?id=' . $modelId]);

        $imagesOptions = [];
        $imgPath = [];

        if (!$model->isNewRecord) {
            $modelId = $model->id;
            $uploadImagesUrl = Url::to(['/blog/upload-images?id=' . $modelId]);
            $imgName = $model->img_name;
            $imgFullPath = Yii::getAlias("@frontend") . "/web/images/blog/" . $imgName;

            if (!empty($imgName)) {
                $deleteUrl = Url::to(["/blog/delete-file?id=" . $modelId]);

                $imgPath[] = Url::to('http://' . $_SERVER['HTTP_HOST'] . '/images/blog/') . $imgName;
                $size = 0;
                if (file_exists($imgFullPath)) {
                    $size = filesize($imgFullPath);
                }
                $imagesOptions[] = [
//                'caption' => $model->title,
                    'url' => $deleteUrl,
                    'size' => $size,
                    'key' => $modelId,
                ];
            }
        }
        ?>

        <?=
        $form->field($model, 'img_name')
            ->widget(
                \kartik\file\FileInput::class,
                [
                    'attribute' => 'img_name',
                    'name' => 'img_name',
                    'options' =>
                        [
                            'accept' => 'image/*',
                            'multiple' => false,
                        ],
                    'pluginOptions' =>
                        [
                            'previewFileType' => 'image',
                            "uploadAsync" => true,
                            'showPreview' => true,
                            'showUpload' => $model->isNewRecord ? false : true,
                            'showCaption' => false,
                            'showDrag' => false,
                            'uploadUrl' => $uploadImagesUrl,
                            'initialPreviewConfig' => $imagesOptions,
                            'initialPreview' => $imgPath,
                            'initialPreviewAsData' => true,
                            'initialPreviewShowDelete' => true,
                            'overwriteInitial' => true,
                            'resizeImages' => true,
                            'layoutTemplates' => [!$model->isNewRecord ?: 'actionUpload' => '',],
                        ],
                ]);
        ?>
    </div>

    <div class="form-group">
        <?= Html::submitButton(Yii::t('app', 'Save'), ['class' => 'btn btn-success']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
