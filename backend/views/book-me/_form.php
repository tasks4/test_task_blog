<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model common\models\BookMe */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="book-me-form">

    <?php $form = ActiveForm::begin(); ?>

    <div class="col-xs-12">
            <?= $form->field($model, 'is_status')
                ->widget(
                    \kartik\switchinput\SwitchInput::classname(),
                    [
                        'value' => true,
                        'pluginOptions' =>
                            [
                                'size' => 'large',
                                'onColor' => 'success',
                                'offColor' => 'danger',
                                'onText' => Yii::t('app', 'Active'),
                                'offText' => Yii::t('app', 'Inactive'),
                            ],
                    ]
                )
            ;
            ?>
    </div>


    <div class="col-xs-12">
        <div class="row">
            <div class="col-xs-6">
                <?= $form->field($model, 'name')->textInput(['maxlength' => true]) ?>
            </div>
            <div class="col-xs-6">
                <?= $form->field($model, 'email')->textInput(['maxlength' => true])?>

            </div>

        </div>
    </div>

    <div class="col-xs-12">
        <?= $form->field($model, 'phone')->textInput(['maxlength' => true]) ?>

    </div>



    <div class="form-group">
        <?= Html::submitButton(Yii::t('app', 'Save'), ['class' => 'btn btn-success']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
