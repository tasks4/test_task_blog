<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model common\models\BookMe */

$this->title = Yii::t('app', 'Create Book Me');
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Book Mes'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="book-me-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
