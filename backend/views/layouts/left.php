<aside class="main-sidebar">

    <section class="sidebar">

        <!-- Sidebar user panel -->
        <div class="user-panel">
            <div class="pull-left image">
                <img src="<?= Yii::$app->homeUrl ?>images/users/man.jpg" class="img-circle" alt="User Image"/>
            </div>
            <div class="pull-left info">
                <p><?= Yii::$app->user->identity['username'] ?></p>

                <a href="#"><i class="fa fa-circle text-success"></i> Online</a>
            </div>
        </div>


        <?= dmstr\widgets\Menu::widget(
            [
                'options' => ['class' => 'sidebar-menu tree', 'data-widget' => 'tree'],
                'items' => [
                    ['label' => Yii::t('app', 'User'), 'icon' => '', 'url' => ['/user/index']],
                    ['label' => Yii::t('app', 'Blog'), 'icon' => '', 'url' => ['/blog/index']],
                    ['label' => Yii::t('app', 'Contact'), 'icon' => '', 'url' => ['/contact/index']],
                    ['label' => Yii::t('app', 'Book Me'), 'icon' => '', 'url' => ['/book-me/index']],

                ],
            ]
        ) ?>

    </section>

</aside>
