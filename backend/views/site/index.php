<?php

/* @var $this yii\web\View */

$this->title = Yii::$app->name;
$homeUrl = Yii::$app->homeUrl;
?>
<div class="container text-center">
    <div class="col-xs-8 img ">
        <h2><strong><?= Yii::$app->name ?></strong><br><br>
            <img src="<?= $homeUrl ?>images/logos.png" class="img-responsive" alt="">
    </div>
</div>
