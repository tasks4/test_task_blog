<?php

namespace common\models;

use Yii;
use yii\behaviors\SluggableBehavior;
use yii\behaviors\TimestampBehavior;

/**
 * This is the model class for table "blog".
 *
 * @property int         $id
 * @property string|null $name
 * @property string|null $slug
 * @property string|null $position
 * @property string|null $seo_title
 * @property string|null $seo_keywords
 * @property string|null $seo_description
 * @property string|null $short_description
 * @property string|null $description
 * @property string|null $search
 * @property string|null $img_name
 * @property string|null $created_at
 * @property string|null $updated_at
 * @property int|null    $is_status
 */
class Blog extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'blog';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['name','description'], 'required'],
            [['description', 'search'], 'string'],
            [['is_status', 'position'], 'integer'],
            [['position'], 'default', 'value' => '0'],
            [
                [
                    'name',
                    'slug',
                    'seo_title',
                    'seo_keywords',
                    'seo_description',
                    'short_description',
                    'img_name',
                    'created_at',
                    'updated_at'
                ],
                'string',
                'max' => 255
            ],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return
            [
                'id'                => Yii::t('app', 'ID'),
                'name'              => Yii::t('app', 'Name'),
                'slug'              => Yii::t('app', 'Slug'),
                'position'          => Yii::t('app', 'Position'),
                'seo_title'         => Yii::t('app', 'Seo Title'),
                'seo_keywords'      => Yii::t('app', 'Seo Keywords'),
                'seo_description'   => Yii::t('app', 'Seo Description'),
                'short_description' => Yii::t('app', 'Short Description'),
                'description'       => Yii::t('app', 'Description'),
                'search'            => Yii::t('app', 'Search'),
                'img_name'          => Yii::t('app', 'Img Name'),
                'created_at'        => Yii::t('app', 'Created At'),
                'updated_at'        => Yii::t('app', 'Updated At'),
                'is_status'         => Yii::t('app', 'Is Status'),
            ];
    }

    /**
     * {@inheritdoc}
     * @return BlogQuery the active query used by this AR class.
     */
    public static function find()
    {
        return new BlogQuery(get_called_class());
    }

    /**
     * @return array|Blog[]
     */
    public static function receiveBlogs()
    {
        return
            self::find()
                ->active()
                ->orderBy(['position' => SORT_DESC, 'created_at' => SORT_DESC])
                ->asArray()
                ->all();
    }

    /**
     * @return array|Blog[]
     */
    public static function receiveBlogDetails(string $slug)
    {
        return self::find()->details($slug)->asArray()->one();
    }

    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            TimestampBehavior::className(),
            [
                'class'         => SluggableBehavior::class,
                'attribute'     => 'name',
                'slugAttribute' => 'slug',
                'immutable'     => true,
                'ensureUnique'  => true
            ],
        ];
    }
}
