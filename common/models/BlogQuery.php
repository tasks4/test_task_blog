<?php

namespace common\models;

/**
 * This is the ActiveQuery class for [[Blog]].
 *
 * @see Blog
 */
class BlogQuery extends \yii\db\ActiveQuery
{
    /**
     * @return BlogQuery
     */
    public function active()
    {
        return $this->andWhere(['is_status' => true]);
    }

    /**
     * @param $slug
     *
     * @return BlogQuery
     */
    public function details($slug)
    {
        return $this->andWhere(['is_status' => true, 'slug' => $slug]);
    }

    /**
     * {@inheritdoc}
     * @return Blog[]|array
     */
    public function all($db = null)
    {
        return parent::all($db);
    }

    /**
     * {@inheritdoc}
     * @return Blog|array|null
     */
    public function one($db = null)
    {
        return parent::one($db);
    }
}
