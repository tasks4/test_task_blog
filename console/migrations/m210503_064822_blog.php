<?php

use yii\db\Migration;

/**
 * Class m210503_064822_blog
 */
class m210503_064822_blog extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {

    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        echo "m210503_064822_blog cannot be reverted.\n";

        return false;
    }

    public function up()
    {
        $tableOptions = '';

        if ($this->db->driverName === 'mysql') {
            $tableOptions = 'CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE=InnoDB';
        }

        $this->createTable('{{%blog}}', [
            'id' => $this->primaryKey(),
            'name' => $this->string(),
            'slug' => $this->string(),
            'position' => $this->string(),
            'seo_title' => $this->string(),
            'seo_keywords' => $this->string(),
            'seo_description' => $this->string(),
            'short_description' => $this->string(),
            'description' => $this->text(),
            'search' => $this->text(600),
            'img_name' => $this->string(),
            'created_at' => $this->string(),
            'updated_at' => $this->string(),
            'is_status' => $this->tinyInteger(2),

        ], $tableOptions);
    }

    public function down()
    {
        $this->dropTable('{{%blog}}');

        return false;
    }
}
