<?php

use yii\db\Migration;

/**
 * Class m210503_091317_contact
 */
class m210503_091317_contact extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {

    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        echo "m210503_091317_contact cannot be reverted.\n";

        return false;
    }


    public function up()
    {
        $tableOptions = '';

        if ($this->db->driverName === 'mysql') {
            $tableOptions = 'CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE=InnoDB';
        }

        $this->createTable('{{%contact}}', [
            'id' => $this->primaryKey(),
            'first_name' => $this->string(),
            'last_name' => $this->string(),
            'email' => $this->string(),
            'message' => $this->string(),


        ], $tableOptions);
    }

    public function down()
    {
        $this->dropTable('{{%contact}}');

        return false;
    }

}
