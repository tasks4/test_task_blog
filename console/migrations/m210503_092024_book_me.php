<?php

use yii\db\Migration;

/**
 * Class m210503_092024_book_me
 */
class m210503_092024_book_me extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {

    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        echo "m210503_092024_book_me cannot be reverted.\n";

        return false;
    }

    public function up()
    {
        $tableOptions = '';

        if ($this->db->driverName === 'mysql') {
            $tableOptions = 'CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE=InnoDB';
        }

        $this->createTable('{{%book_me}}', [
            'id' => $this->primaryKey(),
            'name' => $this->string(),
            'email' => $this->string(),
            'phone' => $this->string(),
            'created_at' => $this->string(),
            'updated_at' => $this->string(),
            'is_status' => $this->tinyInteger(2),


        ], $tableOptions);
    }

    public function down()
    {
        $this->dropTable('{{%contact}}');

        return false;
    }
}
