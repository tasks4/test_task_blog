<?php

namespace frontend\assets;

use yii\web\AssetBundle;
use  Yii;

/**
 * Main frontend application asset bundle.
 */
class AppAsset extends AssetBundle
{
    public $basePath = '@webroot';

    public $baseUrl  = '@web';

    public $css      = [
        'css/main.css',
        "https://fonts.googleapis.com/css?family=Ruthie",
        "https://fonts.googleapis.com/css?family=Roboto",
        "https://fonts.googleapis.com/css?family=Bitter",
        "https://fonts.googleapis.com/css?family=Crimson Text",
        "https://fonts.googleapis.com/css?family=Antic",
        "https://fonts.googleapis.com/css?family=Mulish",
    ];

    public $js       = [
    ];

    public $depends  = [
        'yii\web\YiiAsset',
        'yii\bootstrap\BootstrapAsset',
    ];

    public function __construct($config = [])
    {
        if (Yii::$app->controller->id === 'blog') {
            array_push($this->css, 'css/blog.css');
        }
        if (Yii::$app->controller->id === 'main' and Yii::$app->controller->action->id === 'index') {
            array_push($this->css, 'css/coaching.css');
            array_push($this->js, ['js/book-me.js',], ['js/slider.js']);
        }

        if (Yii::$app->controller->id === 'main' and Yii::$app->controller->action->id === 'contact') {
            array_push($this->css, 'css/contact.css');
        }

        parent::__construct($config);
    }
}
