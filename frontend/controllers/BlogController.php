<?php

namespace frontend\controllers;

use common\models\Blog;
use yii\helpers\Html;
use yii\web\Controller;
use yii\web\NotFoundHttpException;

class BlogController extends Controller
{
    /**
     * @return string
     */
    public function actionIndex()
    {
        $model = Blog::receiveBlogs();

        return $this->render('index', ['model' => $model]);
    }

    /**
     * @return string
     */
    public function actionDetails($slug)
    {
        $blog = Blog::receiveBlogDetails(Html::encode($slug));

        if (empty($blog)) {
            throw new NotFoundHttpException();
        }

        return $this->render('details', ['blog' => $blog]);
    }
}