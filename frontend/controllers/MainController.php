<?php

namespace frontend\controllers;

use common\models\Contact;
use yii\web\Controller;
use yii;

class MainController extends Controller
{
    /**
     * @return string
     */
    public function actionIndex()
    {
        return $this->render('index');
    }

    /**
     * @return string
     */
    public function actionContact()
    {
        $model = new Contact();

        if ($model->load(Yii::$app->request->post()) && $model->validate()) {
            if ($model->sendEmail(Yii::$app->params['adminEmail'])) {
                Yii::$app->session->setFlash(
                    'success',
                    'Thank you for contacting us. We will respond to you as soon as possible.'
                );
            }
            else {
                Yii::$app->session->setFlash('error', 'There was an error sending your message.');
            }
            $model->save();

            return $this->refresh();
        }

        return $this->render('contact', ['model' => $model]);
    }
}