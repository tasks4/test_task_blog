<?php

use  yii\helpers\Url;

/**
 * @var $model \common\models\Blog
 */

$homeUrl = Yii::$app->homeUrl;
?>

<section id="blog-section" class="content">
    <div class="title">
        <h2>Blog</h2>
    </div>
    <div class="blog-content">
        <?php
        foreach ($model as $index => $blog) {
            $pathBlogPage = Url::to(['/blog/details', 'slug' => $blog['slug']]);
            $pathImage    = sprintf('%simages/blog/small/%s', $homeUrl, $blog['img_name']);
            $date         = date('d/m/y', $blog['created_at']);

            ?>
            <div class="blog">
                <div class="blog-photo">
                    <img src="<?= $pathImage ?>" alt="<?= $blog['name'] ?>">
                </div>
                <div class="blog-information">
                    <div class="date">
                        <span><?= $date ?></span>
                    </div>
                    <div class="title">
                        <a href="<?= $pathBlogPage ?>">
                            <?= $blog['name'] ?>
                        </a>
                    </div>
                    <div class="more">
                        <a href="<?= $pathBlogPage ?>">
                            Read More
                        </a>
                    </div>
                </div>
            </div>
            <?php
        }
        ?>

    </div>
</section>
