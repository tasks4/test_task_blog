<?php

use  yii\widgets\ActiveForm;

?>

<section id="lets-connect" class="content">
    <div class="title">
        <h2>Let's Connect</h2>
    </div>
    <div class="lets-connect-content">
        <div class="input-list">
            <div class="social">
                <a href="https://www.instagram.com">
                    <div class="instagram"></div>
                </a>
                <a href="https://www.facebook.com">
                    <div class="facebook"></div>
                </a>
                <a href="https://www.gmail.com">
                    <div class="gmail"></div>
                </a>
            </div>
            <div class="contact">
                <?php $form = ActiveForm::begin(); ?>

                <div class="col-xs-12">
                    <div class="row">
                        <div class="col-xs-6">
                            <?= $form->field($model, 'first_name')->textInput(['maxlength' => true]) ?>
                        </div>
                        <div class="col-xs-6">
                            <?= $form->field($model, 'last_name')->textInput(['maxlength' => true]) ?>

                        </div>

                    </div>
                </div>
                <div class="col-xs-12">
                    <?= $form->field($model, 'email')->textInput(['maxlength' => true]) ?>
                </div>

                <div class="col-xs-12">
                    <?= $form->field($model, 'message')->textarea(['row' => 12]) ?>
                </div>

            </div>
            <div class="send">
                <?= \yii\helpers\Html::submitButton(Yii::t('app', 'Send'), ['class' => 'btn-submit']) ?>

            </div>

            <?php ActiveForm::end(); ?>

        </div>
        <div class="lets-connect-photo">
            <!--      <img src="../assets/images/lets-connect/lets-connect.jpg" alt="Lets-Connect-Photo">-->
        </div>
    </div>
</section>
