<?php

use yii\helpers\Url;

$homeUrl = Yii::$app->homeUrl;
?>

<header id="header">
    <div class="header-content">
        <div class="logo">
            <a href="<?= $homeUrl ?>">
                Sira Arakelyan
            </a>
        </div>
        <div class="menu">
            <ul class="menu-list">
                <li class="menu-item"><a href="<?= $homeUrl ?>">Coaching</a></li>
                <li class="menu-item"><a href="<?= Url::to('/blog') ?>">Blog</a></li>
                <li class="menu-item"><a href="<?= Url::to('/main/contact') ?>">Let's Connect</a></li>
            </ul>
        </div>
        <div class="social">
            <a href="https://www.facebook.com">
                <div class="facebook"></div>
            </a>
            <a href="https://www.instagram.com/">
                <div class="instagram"></div>
            </a>
            <a href="https://www.gmail.com">
                <div class="gmail"></div>
            </a>
        </div>
        <div class="mobile-menu">
            <img src="<?= $homeUrl ?>images/coaching/mobile-menu.png" alt="">
        </div>
    </div>
</header>
